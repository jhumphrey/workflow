local TOKEN="<your_token_here>"
if TOKEN=="<your_token_here>" then
	print("you need to set your token by editing TOKEN in the first line of the file")
	os.exit(1)
end

local json=loadfile("json.lua/json.lua")()
local PROJID="57227382"

local apiurl="https://gitlab.com/api/v4/projects/"..PROJID
local mr = "/merge_requests"

--note you must manually specify space or = before the option
hierarchy={"--request","--request ","--header","--header ","--data","--data "}
function curl(url, flags)
	strtab = {"curl"}
	for i,k in ipairs(hierarchy) do
		if flags[k] then
		v = flags[k]
		table.insert(strtab, " "..k)
		if v and v ~= "" then
			table.insert(strtab,v)
		end
		end
	end
	table.insert(strtab, " "..url)
	table.insert(strtab, " >/tmp/curl.output")
	os.execute(table.concat(strtab))
end

function getMrjson()
	curl(apiurl..mr, {["--header"]=' "PRIVATE-TOKEN: '..TOKEN..'" '})
end

function listRequests()
	local f, err = io.open("/tmp/curl.output", "r")
	if err then
		print(err)
		os.exit(1)
	end
	local results = f:read("*all")
	f:close()
	local rt = json.decode(results)[1]
	for k, v in pairs(rt) do
		io.write(k.."\t"..type(v))
		if type(v) == "string" or type(v)=="number" then
			io.write("\t"..v.."\n")
		elseif type(v) == "table" then
			for jk, jv in pairs(v) do
				io.write(">\t"..jk.."\t"..type(jv))
				if type(jv) == "string" then
					io.write("\t"..jv.."\n")
				else
					io.write("\n")
				end
			end
		else
			io.write("\n")
		end
	end
end

function escape(s)
	s = string.gsub(s,"\"","\\\"")
	return "\""..s.."\""	
end

if arg[1] ~= "-r" then
	getMrjson()
	listRequests()
else
	-- get current branch
	cB, err = io.popen("git branch --show-current", "r")
	if err ~= nil then
		print(err)
		os.exit(1)
	end
	b = cB:read()
	cB:close()
	-- now prompt for title
	io.write("TITLE: ")
	title = io.read()
	-- now prompt for description
	os.execute("vi /tmp/mergerequest.description")
	-- now read description
	descrf, err = io.open("/tmp/mergerequest.description","r")
	if err then
		print(err)
		return 1
	end
	descr = descrf:read("*all")
	descrf:close()
	-- now build the mergetab
	-- {"source_branch":"<branch_name>","target_branch":"<target_branch>","title":"<title>"}'
	mergetab = {source_branch=b,target_branch="main",title=title, description=descr}
	-- now make the request
	curl(apiurl..mr, {["--request"]=" POST",["--header"]=' "PRIVATE-TOKEN: '..TOKEN..'" ',["--header "]='"Content-Type: application/json"',["--data "]=escape(json.encode(mergetab))})
end

os.exit(0)
